import { useState } from "react"
import { useEffect } from "react"
import "./App.css"

const CAT_ENDPOINT_RANDOM_FACT = 'https://catfact.ninja/fact'
const IMAGE_URL = "https://cataas.com"

 /* const CAT_ENDPOINT_IMAGE_URL = `https://cataas.com/cat/says/${threefirstWord}`  */

const App = () => {
    const [fact, setFact] = useState("")
    const [imageURL, setImageURL]= useState()

    useEffect(()=> {
        fetch(CAT_ENDPOINT_RANDOM_FACT)
        .then(res => res.json())
        .then(data => {
            const {fact} = data
            setFact(fact)

        })


    },[])
    //recuperar la imagen con la cita nueva
    useEffect(()=>{
        if(!fact) return

        const threeFirstWord = fact.split(' ', 3)
        .join(" ")
        fetch(`https://cataas.com/cat/says/${threeFirstWord}?size=50&color&red&json=true`)
        .then(res => res.json())
        .then(response => {
            const { url } = response
            console.log(response)
            setImageURL(url)
        })
    }, [fact])

    return (
    <main>

    <h1>GATITOS</h1>
    

    {fact && <p>{fact}</p>}

    {imageURL &&  <img src={`${IMAGE_URL}${imageURL}`} alt={`Image extracted using the first three words for ${fact}`} />}

  
    </main>)
}



export default App